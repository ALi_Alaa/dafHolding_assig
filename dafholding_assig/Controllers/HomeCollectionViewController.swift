//
//  HomeCollectionViewController.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 17/05/2021.
//

import UIKit
import SnapKit
import FirebaseAuth

class HomeCollectionViewController: UIViewController {
    
    //MARK:- Properties
    let headerView = HomeHeaderView()
    
    var homeCollectionView: UICollectionView!
    
    let servicesViewModel = ServiceViewModel.servicesArray
    let reminderViewModel = ReminderViewModel.reminders
    let recommendedViewModel = RecommendedViewModel.recommendedProducts
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        headerView.delegate = self
    }
    
    //MARK:- Selectors
    
    //MARK:- Helpers
    private func configureUI() {
        navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = #colorLiteral(red: 0.9490196078, green: 0.9568627451, blue: 0.968627451, alpha: 1)
        
        self.view.addSubview(headerView)
        headerView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.width.equalToSuperview()
            maker.height.equalTo(130)
            maker.left.right.equalToSuperview()
        }
        
        let frame: CGRect  = CGRect(x: 0, y: 135, width: SCREEN_WIDTH, height: self.view.frame.size.height - 135)
        homeCollectionView = UICollectionView(frame: frame, collectionViewLayout: createCompositionalLayout())
        homeCollectionView.backgroundColor = .clear
        homeCollectionView.delegate = self
        homeCollectionView.dataSource = self
        self.view.addSubview(homeCollectionView)
        
        registerationsCell(cells: [TrendingCollectionViewCell.self,
                                   ServicesCollectionViewCell.self,
                                   ReminderCollectionViewCell.self,
                                   RecommendedCollectionViewCell.self]
                           ,
                           reuseID: [
                            TrendingCollectionViewCell.identifier,
                            ServicesCollectionViewCell.identifier,
                            ReminderCollectionViewCell.identifier,
                            RecommendedCollectionViewCell.identifier
                           ])
    }

}

//MARK:- Signout Delegate
extension HomeCollectionViewController: HomeHeaderDelegate {
    func UserSignOut() {
        let navigation = UINavigationController(rootViewController: AuthViewController())
        navigation.modalPresentationStyle = .fullScreen
        navigation.isNavigationBarHidden = true
        self.present(navigation, animated: false, completion: nil)
    }
}

//MARK:- func collectionView layout
extension HomeCollectionViewController {
    
    /// generate the section with it's own dimension
    private func createCompositionalLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { [ weak self ] sectionNumber, layoutEnvironment in
            switch sectionNumber {
            case 0:
                return self?.setupCollectionCompositionLayout(scrollType: .groupPagingCentered, width: .fractionalWidth(1), height: .absolute(170), itemTrailing: 12, sectionLeading: 0)
            case 1:
                return self?.setupCollectionCompositionLayout(scrollType: .continuousGroupLeadingBoundary,
                                                              width: .fractionalWidth(0.25), height: .fractionalWidth(0.25),
                                                              itemTrailing: 12, sectionLeading: 0)
            case 2:
                return self?.setupCollectionCompositionLayout(scrollType: .continuousGroupLeadingBoundary,
                                                              width: .fractionalWidth(0.93), height: .fractionalWidth(0.4),
                                                              itemTrailing: 12, horizontal: false, sectionLeading: 0)
            case 3, 4:
                return self?.setupCollectionCompositionLayout(scrollType: .groupPaging,
                                                              width: .fractionalWidth(0.45), height: .fractionalWidth(0.45),
                                                              itemTrailing: 12, horizontal: true, sectionLeading: 0)
            default:
                return self?.setupCollectionCompositionLayout(scrollType: .groupPagingCentered, width: .fractionalWidth(0.98), height: .absolute(170))
            }
        }
        
        let configure = UICollectionViewCompositionalLayoutConfiguration()
        configure.interSectionSpacing = 28
        layout.configuration = configure
        return layout
        
    }
    
    /// create compostion layout section -> group -> item
    private func setupCollectionCompositionLayout(scrollType: UICollectionLayoutSectionOrthogonalScrollingBehavior,
                                                  width: NSCollectionLayoutDimension,
                                                  height: NSCollectionLayoutDimension,
                                                  itemTrailing: CGFloat = 15,
                                                  horizontal: Bool = true,
                                                  sectionLeading: CGFloat = 0) -> NSCollectionLayoutSection {
        let item: NSCollectionLayoutItem!
        
        if horizontal {
            item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1)))
        }else {
            item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(0.5)))
            item.contentInsets.bottom = 8
        }
        item.contentInsets.trailing = itemTrailing
        item.contentInsets.leading = itemTrailing
        
        let group: NSCollectionLayoutGroup!
        
        if horizontal {
            group = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: width, heightDimension: height), subitems: [item])
        } else {
            group = NSCollectionLayoutGroup.vertical(layoutSize: NSCollectionLayoutSize(widthDimension: width, heightDimension: height), subitems: [item])
        }
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = scrollType
        section.contentInsets.leading = sectionLeading
        
        let  layoutSectionHeader = createSectionHeader(elementKind: UICollectionView.elementKindSectionHeader)
        section.boundarySupplementaryItems = [layoutSectionHeader]
        
        return section
        
    }
    
    /// create section header for collection view
    private func createSectionHeader(elementKind: String) -> NSCollectionLayoutBoundarySupplementaryItem {
        let layoutSectionHeaderSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(40))
        let layoutSectionHeader = NSCollectionLayoutBoundarySupplementaryItem(layoutSize:layoutSectionHeaderSize,
                                                                              elementKind: elementKind,
                                                                              alignment: .topLeading)
        
        return layoutSectionHeader
    }
    
    private func registerationsCell(cells: [AnyClass], reuseID: [String]) {
        homeCollectionView.register(HomeHeaderCollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: HomeHeaderCollectionReusableView.identifier)
        
        var number = 0
        for cell in cells {
            number += 1
            homeCollectionView.register(cell, forCellWithReuseIdentifier: reuseID[number - 1])
        }
        
    }
    
}

//MARK:- Collection view Delegate & Collection view DataSource
extension HomeCollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: HomeHeaderCollectionReusableView.identifier, for: indexPath) as! HomeHeaderCollectionReusableView
        
        switch indexPath.section {
        case 0:
            sectionHeader.title.text = "Trending"
        case 1:
            sectionHeader.title.text = "Our Services"
        case 2:
            sectionHeader.title.text = "Reminders"
        case 3:
            sectionHeader.title.text = "Recommended For You"
        case 4:
            sectionHeader.title.text = "Most Selling"
        default:
            sectionHeader.title.text = ""
        }
        
        return sectionHeader
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return servicesViewModel.count
        case 2:
            return reminderViewModel.count
        case 3:
            return recommendedViewModel.count
        case 4:
            return 6
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        
        switch indexPath.section {
        case 0:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: TrendingCollectionViewCell.identifier, for: indexPath) as! TrendingCollectionViewCell
            
        case 1:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: ServicesCollectionViewCell.identifier, for: indexPath) as! ServicesCollectionViewCell
            let model = servicesViewModel[indexPath.row]
            (cell as! ServicesCollectionViewCell).serviceViewModel = model
            
        case 2:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReminderCollectionViewCell.identifier, for: indexPath) as! ReminderCollectionViewCell
            let model = reminderViewModel[indexPath.row]
            (cell as! ReminderCollectionViewCell).reminderViewModel = model
            
        case 3:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: RecommendedCollectionViewCell.identifier, for: indexPath) as! RecommendedCollectionViewCell
            let model = recommendedViewModel[indexPath.row]
            (cell as! RecommendedCollectionViewCell).recommendedViewModel = model
            
        case 4:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: RecommendedCollectionViewCell.identifier, for: indexPath) as! RecommendedCollectionViewCell
            
        default:
            cell = UICollectionViewCell()
        }
        return cell
    }
    
    
}
