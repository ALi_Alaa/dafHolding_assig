//
//  UserViewModel.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 19/05/2021.
//

import Foundation

struct UserViewModel {
    
    let userName: String
    
    init(model: UserModel) {
        self.userName = model.userName
    }
    
}
