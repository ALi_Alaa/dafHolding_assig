//
//  ErrorHandler.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 17/05/2021.
//

import UIKit
import SnapKit

class ErrorHandler {
    
    // MARK: - Properties
    static let shared = ErrorHandler()
    
    let backGroundView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 12
        view.clipsToBounds = true
        return view
    }()
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.tintColor = .darkGray
        return iv
    }()
    
    let label: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = .white
        lbl.textAlignment = .left
        return lbl
    }()
    
    // MARK: - Helpers
    func showError(textLabel: String, alertColor: UIColor, image: String? = "", on view: UIView, top: Bool) {
        guard let imageString = image else {
            return
        }
        view.addSubview(backGroundView)
        backGroundView.backgroundColor = alertColor
        backGroundView.layer.opacity = 0
        backGroundView.layer.zPosition = 10
        backGroundView.bringSubviewToFront(view)
        
        if top {
            backGroundView.snp.makeConstraints { (maker) in
                maker.width.equalToSuperview().inset(15)
                maker.height.equalTo(60)
                maker.top.equalToSuperview()
                maker.centerX.equalToSuperview()
            }
            
            UIView.animate(withDuration: 0.6) {
                self.backGroundView.transform = CGAffineTransform(translationX: 0, y: notch)
                self.backGroundView.layer.opacity = 1
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.dismiss()
                }
            }
            
        } else {
            backGroundView.snp.makeConstraints { (maker) in
                maker.width.equalToSuperview().inset(15)
                maker.height.equalTo(60)
                maker.bottom.equalToSuperview()
                maker.centerX.equalToSuperview()
            }
            
            UIView.animate(withDuration: 0.6) {
                self.backGroundView.transform = CGAffineTransform(translationX: 0, y: -80)
                self.backGroundView.layer.opacity = 1
                DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                    self.dismiss()
                }
            }
        }
        
        imageView.snp.makeConstraints { (maker) in
            maker.width.height.equalTo(24)
        }
        
        let stack = UIStackView(arrangedSubviews: [imageView, label])
        stack.spacing = 8
        stack.distribution = .fillProportionally
        
        imageView.image = UIImage(systemName: imageString)
        imageView.tintColor = .white
        label.text = textLabel
        backGroundView.addSubview(stack)
        stack.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
            maker.width.equalToSuperview().inset(15)
        }
        
        
    }
    
    func dismiss() {
        UIView.animate(withDuration: 0.6) {
            self.backGroundView.transform = CGAffineTransform(translationX: 0, y: 0)
            self.backGroundView.layer.opacity = 0
        }
    }
    
}
