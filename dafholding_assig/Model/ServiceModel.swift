//
//  ServiceModel.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 19/05/2021.
//

import Foundation

struct ServiceModel {
    let serviceImage: String
    let serviceTitle: String
}
