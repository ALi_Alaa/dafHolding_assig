//
//  SignUpView.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 17/05/2021.
//

import UIKit
import FirebaseAuth


protocol SignupDelegate {
    func signUpProgress()
    func errorInSignUp(error: AuthErrorCode?)
}

class SignUpView: UIView {

    //MARK:- Delegate
    var delegate: SignupDelegate?
    
    //MARK:- Properties
    let scrollView: UIScrollView = {
      let scroll = UIScrollView()
        return scroll
    }()
    
    let spinner = UIActivityIndicatorView(style: .gray)
    
    let userNameLabel = createLabel(labelTitle: "NAME", fontSize: 12, fontWeight: .medium)
    let userNameTextField = createTextField(placeHolderText: "full name...")
    
    let phoneNumberLabel  = createLabel(labelTitle: "PHONE NUMBER", fontSize: 12, fontWeight: .medium)
    let phoneNumberTtextField = createTextField(placeHolderText: "phone number")
    
    let addressEmailLabel = createLabel(labelTitle: "EMAIL ADDRESS", fontSize: 12, fontWeight: .medium)
    let emailTextField = createTextField(placeHolderText: "Write your email...")
    
    let passwordLabel = createLabel(labelTitle: "PASSWORD", fontSize: 12, fontWeight: .medium)
    let passwordTextField = createTextField(placeHolderText: "your password...")
    
    let addressLabel = createLabel(labelTitle: "ADDRESS", fontSize: 12, fontWeight: .medium)
    let addressLabelTextField = createTextField(placeHolderText: "write your address...")
    
    let checkBoxButton: UIButton = {
      let checkbtn = UIButton()
        checkbtn.setImage(UIImage(systemName: "rectangle"), for: .normal)
        checkbtn.imageView?.contentMode = .scaleAspectFit
        checkbtn.imageView?.tintColor = .darkGray
        checkbtn.addTarget(self, action: #selector(checkBoxTapped), for: .touchUpInside)
        return checkbtn
    }()
    let checkBoxLabel = createLabel(labelTitle: "I have read and agree to the ", fontSize: 14, fontWeight: .medium)
    let checkBoxLabelLink = createLabel(labelTitle: "Terms and conditions", fontSize: 12, fontWeight: .bold)
    
    let signUpButton: UIButton = {
      let btn = UIButton()
        btn.setTitle("Sign Up", for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        btn.backgroundColor = #colorLiteral(red: 0.05490196078, green: 0.4117647059, blue: 0.6549019608, alpha: 1)
        btn.layer.cornerRadius = 11
        btn.addTarget(self, action: #selector(signUpTapped), for: .touchUpInside)
        return btn
    }()
    
    var isChecked: Bool = false

    
    //MARK:- Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
    
    //MARK:- Selectors
    @objc
    func checkBoxTapped() {
        isChecked = !isChecked
        if isChecked {
            checkBoxButton.setImage(UIImage(systemName: "checkmark.rectangle"), for: .normal)
        } else {
            checkBoxButton.setImage(UIImage(systemName: "rectangle"), for: .normal)
        }
    }
    
    @objc
    func signUpTapped() {
        signUpButton.setTitle("", for: .normal)
        spinner.startAnimating()
        guard !emailTextField.text!.isEmpty, !passwordTextField.text!.isEmpty, !userNameTextField.text!.isEmpty, !phoneNumberTtextField.text!.isEmpty,
              let email = emailTextField.text, let password = passwordTextField.text,
              let userName = userNameTextField.text, let phoneNumber = phoneNumberTtextField.text,
              let address = addressLabelTextField.text
        else {
            ErrorHandler.shared.showError(textLabel: "All Fields Required", alertColor: .systemRed,
                                          image: "exclamationmark.triangle.fill", on: self, top: false)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.spinner.stopAnimating()
                self.signUpButton.setTitle("Sign Up", for: .normal)
            }
            return
        }
        
        guard isChecked else {
            ErrorHandler.shared.showError(textLabel: "Please, check terms and conditions", alertColor: .systemRed,
                                          image: "exclamationmark.triangle.fill", on: self, top: false)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.spinner.stopAnimating()
                self.signUpButton.setTitle("Sign Up", for: .normal)
            }
            return
        }
        
        let value = ["UserName" : userName, "PhoneNumber" : phoneNumber, "Email" : email, "Address" : address]
        
        AuthCaller.shared.signUpUser(email: email, password: password, value: value) { [weak  self] result in
            switch result {
            case .failure(let error):
                let err = AuthErrorCode(rawValue: error._code)
                self?.delegate?.errorInSignUp(error: err)
            case .success(_):
                print("DEBUG: success")
                guard let window = UIApplication.shared.windows.first(where: {$0.isKeyWindow}),
                      let rootController = window.rootViewController as? MainTabController else {return}
                rootController.userCurrentState()
                self?.delegate?.signUpProgress()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self?.spinner.stopAnimating()
                self?.signUpButton.setTitle("Sign Up", for: .normal)
            }
        }
    }
    
    //MARK:- Helpers
    private func configureUI() {
        self.addSubview(scrollView)
        scrollView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.left.right.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
        
        scrollView.addSubview(userNameLabel)
        userNameLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(40)
            maker.width.equalToSuperview()
            maker.left.equalTo(21)
        }
        
        scrollView.addSubview(userNameTextField)
        userNameTextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(userNameLabel.snp.bottom)
            maker.width.equalToSuperview()
            maker.height.equalTo(40)
            maker.left.equalTo(21)
        }
        userNameTextField.addUnderLine(width: self.frame.width - 40)
        
        scrollView.addSubview(phoneNumberLabel)
        phoneNumberLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(userNameTextField.snp.bottom).offset(33)
            maker.width.equalToSuperview()
            maker.left.equalTo(21)
        }
        
        scrollView.addSubview(phoneNumberTtextField)
        phoneNumberTtextField.keyboardType = .numberPad
        phoneNumberTtextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(phoneNumberLabel.snp.bottom)
            maker.width.equalToSuperview()
            maker.height.equalTo(40)
            maker.left.equalTo(21)
        }
        phoneNumberTtextField.addUnderLine(width: self.frame.width - 40)
        
        scrollView.addSubview(addressEmailLabel)
        addressEmailLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(phoneNumberTtextField.snp.bottom).offset(33)
            maker.width.equalToSuperview()
            maker.left.equalTo(21)
        }
        
        scrollView.addSubview(emailTextField)
        emailTextField.keyboardType = .emailAddress
        emailTextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(addressEmailLabel.snp.bottom)
            maker.width.equalToSuperview()
            maker.height.equalTo(40)
            maker.left.equalTo(21)
        }
        emailTextField.addUnderLine(width: self.frame.width - 40)
        
        scrollView.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(emailTextField.snp.bottom).offset(33)
            maker.width.equalToSuperview()
            maker.left.equalTo(21)
        }
        
        scrollView.addSubview(passwordTextField)
        passwordTextField.isSecureTextEntry = true
        passwordTextField.keyboardType = .asciiCapable
        passwordTextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(passwordLabel.snp.bottom)
            maker.width.equalToSuperview()
            maker.height.equalTo(40)
            maker.left.equalTo(21)
        }
        passwordTextField.addUnderLine(width: self.frame.width - 40)
        
        scrollView.addSubview(addressLabel)
        addressLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(passwordTextField.snp.bottom).offset(33)
            maker.width.equalToSuperview()
            maker.left.equalTo(21)
        }
        
        scrollView.addSubview(addressLabelTextField)
        addressLabelTextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(addressLabel.snp.bottom)
            maker.width.equalToSuperview()
            maker.height.equalTo(40)
            maker.left.equalTo(21)
        }
        addressLabelTextField.addUnderLine(width: self.frame.width - 40)
        
        let checkBoxStack = UIStackView(arrangedSubviews: [checkBoxButton, checkBoxLabel, checkBoxLabelLink])
        checkBoxStack.alignment = .leading
        
        checkBoxButton.snp.makeConstraints { (maker) in
            maker.width.height.equalTo(30)
        }
        
        checkBoxLabel.snp.makeConstraints { (maker) in
            maker.height.equalTo(30)
        }
        checkBoxLabelLink.textColor = .systemBlue
        checkBoxLabelLink.snp.makeConstraints { (maker) in
            maker.height.equalTo(30)
        }
        
        scrollView.addSubview(checkBoxStack)
        checkBoxStack.snp.makeConstraints { (maker) in
            maker.top.equalTo(addressLabelTextField.snp.bottom).offset(33)
            maker.left.equalTo(21)
        }
        
        scrollView.addSubview(signUpButton)
        signUpButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(checkBoxStack.snp.bottom).offset(33)
            maker.centerX.equalToSuperview()
            maker.width.equalTo(340)
            maker.height.equalTo(57)
        }
        
        signUpButton.addSubview(spinner)
        spinner.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        
    }

}
