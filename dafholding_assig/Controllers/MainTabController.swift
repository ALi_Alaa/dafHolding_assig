//
//  MainTabController.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 17/05/2021.
//

import UIKit
import FirebaseAuth

class MainTabController: UITabBarController {
    
    //MARK:- Properties
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        userCurrentState()
    }
    
    
    //MARK:- Helpers
    func userCurrentState() {
        let userState = Auth.auth().currentUser
        if userState == nil {
            DispatchQueue.main.async {
                let navigation = UINavigationController(rootViewController: AuthViewController())
                navigation.modalPresentationStyle = .fullScreen
                self.present(navigation, animated: false, completion: nil)
            }
        }else {
            print("DEBUG: user has been successfully logged in")
            ConfigureViewControllers()
        }
    }
    
    func ConfigureViewControllers() {
        let home = HomeCollectionViewController()
        let nav1 = ConfigureNavigationController(imageName: "house.fill", labelNav: "Home", nav: home)
        
        let record = ExploreViewController()
        record.title = "Record"
        let nav2 = ConfigureNavigationController(imageName: "record",asset: true, labelNav: "Record", nav: record)
        
        let explore = ExploreViewController()
        explore.title = "Explore"
        let nav3 = ConfigureNavigationController(imageName: "explore",asset: true, labelNav: "Explore", nav: explore)
        
        let insurance = ExploreViewController()
        insurance.title = "Insurance"
        let nav4 = ConfigureNavigationController(imageName: "insurance",asset: true, labelNav: "Insurance", nav: insurance)
        
        let profile = ProfileViewController()
        profile.title = "Profile"
        let nav5 = ConfigureNavigationController(imageName: "person.fill", labelNav: "Profile", nav: profile)
        
        viewControllers = [nav1, nav2, nav3, nav4, nav5]
    }
    
    /// function configure navigation controller 
    func ConfigureNavigationController(imageName: String,asset: Bool = false, labelNav: String, nav: UIViewController) -> UINavigationController {
        let navigation = UINavigationController(rootViewController: nav)
        if !asset {
            navigation.tabBarItem.image = UIImage(systemName: imageName)
        } else {
            navigation.tabBarItem.image = UIImage(named: imageName)
        }
        navigation.title = labelNav
        return navigation
    }
}
