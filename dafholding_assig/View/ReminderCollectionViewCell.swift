//
//  ReminderCollectionViewCell.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 18/05/2021.
//

import UIKit

class ReminderCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Identifier
    static let identifier = "Reminder"
    
    var reminderViewModel: ReminderViewModel! {
        didSet {
            self.reminderImage.image = UIImage(named: reminderViewModel.reminderImage)
            self.reminderTitle.text = reminderViewModel.reminderTitle
            self.reminderSubTitle.text = reminderViewModel.reminderSubTitle
        }
    }
    
    //MARK:- Properties
    let background = UIView()
    
    let reminderImage: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = #imageLiteral(resourceName: "trending").withRenderingMode(.alwaysOriginal)
        img.clipsToBounds = true
        return img
    }()
    
    let reminderTitle = createLabel(labelTitle: "", fontSize: 13, fontWeight: .bold)
    let reminderSubTitle = createLabel(labelTitle: "", fontSize: 10, fontWeight: .regular)
    
    //MARK:- Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK:- Helpers
    private func configureUI() {
        addSubview(background)
        background.backgroundColor = .white
        background.layer.cornerRadius = 8
        background.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.width.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
        
        background.addSubview(reminderImage)
        reminderImage.snp.makeConstraints { (maker) in
            maker.left.equalToSuperview().offset(10)
            maker.top.equalTo(10)
            maker.bottom.equalTo(-10)
            maker.width.equalTo(background.snp.height).offset(-20)
        }
        
        let reminderTextStack = UIStackView(arrangedSubviews: [reminderTitle, reminderSubTitle])
        reminderTextStack.alignment = .leading
        reminderTextStack.axis = .vertical
        reminderTextStack.spacing = 8
        
        background.addSubview(reminderTextStack)
        reminderTitle.text = "Dose Reminder"
        reminderSubTitle.text = "Next Dosage"
        reminderTitle.textAlignment = .left
        reminderSubTitle.textAlignment = .left
        reminderTitle.textColor = .black
        reminderTextStack.snp.makeConstraints { (maker) in
            maker.width.equalToSuperview()
            maker.centerY.equalToSuperview()
            maker.left.equalTo(reminderImage.snp.right).offset(10)
        }
        
    }
    
}
