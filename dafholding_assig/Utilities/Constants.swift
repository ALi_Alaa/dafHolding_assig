//
//  Constants.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 18/05/2021.
//

import FirebaseDatabase
import FirebaseAuth
import UIKit

let REF_DATABASE = Database.database().reference()
let REF_USERS = REF_DATABASE.child("Users")
let REF_SERVICES = REF_DATABASE.child("Services")
let CURRENT_USER = Auth.auth().currentUser?.uid

var notch: CGFloat {
    let top = UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 0
    return top
}
