//
//  AuthCaller.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 18/05/2021.
//

import UIKit
import FirebaseAuth
import Firebase

class AuthCaller: NSObject {
    
    static let shared = AuthCaller()
    
    /// sign up function for new users
    func signUpUser(email: String, password: String, value: [String: Any], completion: @escaping (Result<Any, Error>) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { result, error in
            guard error == nil else {
                completion(.failure(error!))
                return
            }
            
            REF_USERS.child((result?.user.uid)!).updateChildValues(value) { (error, ref) in
                print("DEBUG: success in add user value: \(ref)")
                completion(.success(ref))
            }
        }
    }
    
    /// sign in existing users
    func loginUser(email: String, password: String, completion: @escaping (Result<Any, Error>) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { result, error in
            guard error == nil else {
                completion(.failure(error!))
                return
            }
            guard let result = result else {return}
            print("DEBUG: user is successfully can access")
            completion(.success(result))
        }
    }
    
    func fetchUser(completion: @escaping(UserViewModel) -> Void) {
        guard let userID = CURRENT_USER else {return}
        REF_USERS.child(userID).observe(.value) { snapshot in
            guard let snap = snapshot.value as? NSDictionary, let name = snap["UserName"] as? String else {return}
            print("DEBUG: user snap is: \(name)")
            let userModel = UserViewModel(model: UserModel(userName: name))
            completion(userModel)
        }
    }
 
}
