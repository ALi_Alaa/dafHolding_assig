//
//  HomeHeaderView.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 18/05/2021.
//

import UIKit
import SnapKit
import FirebaseAuth

protocol HomeHeaderDelegate {
    func UserSignOut()
}

class HomeHeaderView: UIView {
    
    //MARK:- Delegate
    var delegate: HomeHeaderDelegate?
    
    //MARK:- Propeties
    var userName: String = ""
    
    var userViewModel: UserViewModel! {
        didSet {
            userName = userViewModel.userName
            messageHeaderUpdate()
        }
    }
    
    var currentDate = Calendar.current.component(.hour, from: Date())
    
    let profileUserImage: UIImageView = {
      let img = UIImageView()
        img.layer.cornerRadius = 13
        img.clipsToBounds = true
        img.image = #imageLiteral(resourceName: "facebook_daf")
        img.contentMode = .scaleAspectFill
        img.tintColor = .none
        return img
    }()
    
    let welcomeText = createLabel(labelTitle: "", fontSize: 12, fontWeight: .bold)
    
    let burgerMenuIcon: UIButton = {
      let btn = UIButton()
        btn.setImage(UIImage(systemName: "line.horizontal.3"), for: .normal)
        btn.tintColor = .none
        btn.imageView?.contentMode = .scaleAspectFill
        btn.addTarget(self, action: #selector(logoutTapped), for: .touchUpInside)
        return btn
    }()
    
    let searchBar: UISearchBar = {
      let searchBar = UISearchBar()
        searchBar.placeholder = "What are you looking for ?"
        searchBar.searchBarStyle = .minimal
        searchBar.sizeToFit()
        searchBar.searchTextField.textAlignment = .center
        searchBar.layer.cornerRadius = 14
        return searchBar
    }()
    
    //MARK:- Lifecycle
    override func layoutSubviews() {
        getUser()
        configureUI()
    }
    
    //MARK:- API
    func getUser() {
        AuthCaller.shared.fetchUser { [ weak self ] model in
            self?.userViewModel = model
        }
    }
    
    //MARK:- Selectors
    @objc
    func logoutTapped() {
        do {
            try Auth.auth().signOut()
            DispatchQueue.main.async {
                self.delegate?.UserSignOut()
            }
        }
        catch {
            print("DEBUG: can't signOut user")
        }
    }
    
    
    //MARK:- Helpers
    private func configureUI() {
        backgroundColor = .clear
        let headerStack = UIStackView(arrangedSubviews: [profileUserImage, welcomeText, burgerMenuIcon])
        headerStack.spacing = 14
        
        profileUserImage.snp.makeConstraints { (maker) in
            maker.width.height.equalTo(26)
        }
        
        welcomeText.textColor = .black
        
        burgerMenuIcon.snp.makeConstraints { (maker) in
            maker.right.equalToSuperview()
            maker.width.equalTo(18)
        }
        
        self.addSubview(headerStack)
        headerStack.snp.makeConstraints { maker in
            maker.top.equalTo(48)
            maker.left.equalTo(21)
            maker.right.equalTo(-21)
        }
        
        self.addSubview(searchBar)
        searchBar.snp.makeConstraints { (maker) in
            maker.bottom.equalToSuperview().offset(-6)
            maker.height.equalTo(38)
            maker.centerX.equalToSuperview()
            maker.left.equalTo(21)
            maker.right.equalTo(-21)
        }
        
    }
    
    private func messageHeaderUpdate() {
        switch currentDate {
        case 6..<12:
            welcomeText.text = "\(CurrentMessageFromHours.morning.rawValue), \(userViewModel.userName)"
        case 12:
            welcomeText.text = "\(CurrentMessageFromHours.noon.rawValue), \(userViewModel.userName)"
        case 13..<17:
            welcomeText.text = "\(CurrentMessageFromHours.Afternoon.rawValue), \(userViewModel.userName)"
        case 17..<22:
            welcomeText.text = "\(CurrentMessageFromHours.evening.rawValue), \(userViewModel.userName)"
        default:
            welcomeText.text = "Night"
        }
    }
}
