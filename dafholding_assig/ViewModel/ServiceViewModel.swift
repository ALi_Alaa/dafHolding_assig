//
//  ServiceViewModel.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 19/05/2021.
//

import Foundation

struct ServiceViewModel {
    
    let serviceImage: String
    let serviceTitle: String
    
    init(service: ServiceModel) {
        self.serviceImage = service.serviceImage
        self.serviceTitle = service.serviceTitle
    }
    
    static let servicesArray = [
        ServiceViewModel(service: ServiceModel(serviceImage: "products", serviceTitle: "products")),
        ServiceViewModel(service: ServiceModel(serviceImage: "labs", serviceTitle: "labs")),
        ServiceViewModel(service: ServiceModel(serviceImage: "doctors", serviceTitle: "doctors")),
        ServiceViewModel(service: ServiceModel(serviceImage: "hospitals", serviceTitle: "hospitals"))
    ]
    
}
