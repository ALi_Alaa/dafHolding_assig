//
//  AuthViewController.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 17/05/2021.
//

import UIKit
import SnapKit
import FirebaseAuth

class AuthViewController: UIViewController {

    //MARK:- Properties
    let headerImage: UIImageView = {
      let img = UIImageView()
        img.image = #imageLiteral(resourceName: "Auth_background")
        img.contentMode = .scaleAspectFill
        img.clipsToBounds = true
        return img
    }()
    
    let backButton: UIButton = {
      let btn = UIButton()
        btn.setTitle("BACK", for: .normal)
        btn.addTarget(self, action: #selector(backTapped), for: .touchUpInside)
        return btn
    }()
    
    let loginView = LoginView()
    let signupView = SignUpView()
    
    //MARK:- Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        hideKeyboardWhenTappedAround(with: self.view)
        loginView.delegate = self
        signupView.delegate = self
        registerNotifications()
    }
    
    private func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    //MARK:- Selectors
    @objc
    func backTapped() {
        UIView.animate(withDuration: 0.4) {
            self.loginView.transform = .identity
            self.signupView.transform = .identity
            self.backButton.layer.opacity = 0
        }
    }
    
    @objc private func keyboardWillShow(notification: NSNotification){
        guard let keyboardFrame = notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        signupView.scrollView.contentSize = CGSize(width: SCREEN_WIDTH, height: UIScreen.main.bounds.height - view.convert(keyboardFrame.cgRectValue, from: nil).size.height)
        signupView.scrollView.contentInset.bottom = view.convert(keyboardFrame.cgRectValue, from: nil).size.height
    }

    @objc private func keyboardWillHide(notification: NSNotification){
        signupView.scrollView.contentInset.bottom = 0
        signupView.scrollView.contentSize = CGSize(width: SCREEN_WIDTH, height: 0)
    }
    
    //MARK:- Helpers
    private func configureUI() {
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = .white
        
        self.view.addSubview(headerImage)
        headerImage.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.width.equalToSuperview()
            maker.height.equalTo(143)
        }
        
        self.view.addSubview(backButton)
        backButton.layer.zPosition = 10
        backButton.layer.opacity = 0
        backButton.snp.makeConstraints { (maker) in
            maker.centerX.equalToSuperview()
            maker.width.height.equalTo(90)
            maker.top.equalTo(60)
        }
        
        self.view.addSubview(loginView)
        loginView.snp.makeConstraints { (maker) in
            maker.top.equalTo(headerImage.snp.bottom)
            maker.width.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
    
        self.view.addSubview(signupView)
        signupView.snp.makeConstraints { (maker) in
            maker.top.equalTo(headerImage.snp.bottom)
            maker.width.equalToSuperview()
            maker.left.equalTo(SCREEN_WIDTH)
            maker.bottom.equalToSuperview()
        }
    }

}

//MARK:- Login delegate to show sign up page
extension AuthViewController: LoginDelegate {
    func logInApiCallErrorDelegate(error: AuthErrorCode?) {
        switch error {
        case .networkError:
            print("DEBUG: there is an error in interent connection")
            ErrorHandler.shared.showError(textLabel: "No Internet Connection 😳", alertColor: .systemRed, image: "exclamationmark.triangle.fill", on: self.view, top: true)
        case .invalidEmail:
            print("DEBUG: this email is invalid email")
            ErrorHandler.shared.showError(textLabel: "Invalid Email 🌚", alertColor: .systemRed, image: "exclamationmark.triangle.fill", on: self.view, top: true)
        case .emailAlreadyInUse:
            ErrorHandler.shared.showError(textLabel: "Email already exist 😒", alertColor: .systemRed, image: "exclamationmark.triangle.fill", on: self.view, top: true)
        case .userNotFound:
            print("DEBUG: user not found")
            ErrorHandler.shared.showError(textLabel: "No user with this email 🦦", alertColor: .systemRed, image: "exclamationmark.triangle.fill", on: self.view, top: true)
        case .wrongPassword:
            print("DEBUG: Wrong password")
            ErrorHandler.shared.showError(textLabel: "Wrong password 🌚", alertColor: .systemRed, image: "exclamationmark.triangle.fill", on: self.view, top: true)
        default:
            break
        }
    }
    
    func logInSuccessDelegate() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func signupTappedDelegate() {
        UIView.animate(withDuration: 0.4) {
            self.loginView.transform = CGAffineTransform(translationX: -SCREEN_WIDTH, y: 0)
            self.signupView.transform = CGAffineTransform(translationX: -SCREEN_WIDTH, y: 0)
            self.backButton.layer.opacity = 1
        }
    }
 
}

//MARK:- Signup delegate to hide Auth page or show error
extension AuthViewController: SignupDelegate {
    func signUpProgress() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func errorInSignUp(error: AuthErrorCode?) {
        switch error {
        case .networkError:
            print("DEBUG: there is an error in interent connection")
            ErrorHandler.shared.showError(textLabel: "No Internet Connection 😳", alertColor: .systemRed, image: "exclamationmark.triangle.fill", on: self.view, top: true)
        case .invalidEmail:
            print("DEBUG: this email is invalid email")
            ErrorHandler.shared.showError(textLabel: "Invalid Email 🌚", alertColor: .systemRed, image: "exclamationmark.triangle.fill", on: self.view, top: true)
        case .emailAlreadyInUse:
            ErrorHandler.shared.showError(textLabel: "Email already exist 😒", alertColor: .systemRed, image: "exclamationmark.triangle.fill", on: self.view, top: true)
        case .weakPassword:
            print("DEBUG: this password is so weak")
            ErrorHandler.shared.showError(textLabel: "Weak password 🦦", alertColor: .systemRed, image: "exclamationmark.triangle.fill", on: self.view, top: true)
        default:
            break
        }
    }
}
