//
//  HomeHeaderCollectionReusableView.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 18/05/2021.
//

import UIKit
import SnapKit

class HomeHeaderCollectionReusableView: UICollectionReusableView {
      
    //MARK:- Identifier
    static let identifier = "sectionHeader"
    
    //MARK:- Properties
    var title = createLabel(labelTitle: "", fontSize: 18, fontWeight: .bold)
    
    //MARK:- Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(title)
        title.textColor = .black
        title.snp.makeConstraints { (maker) in
            maker.left.equalTo(12)
            maker.top.equalToSuperview()
            maker.bottom.equalToSuperview().offset(-10)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
