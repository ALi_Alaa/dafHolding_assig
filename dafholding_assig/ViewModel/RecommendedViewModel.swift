//
//  RecommendedViewModel.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 19/05/2021.
//

import Foundation

struct RecommendedViewModel {
    
    let productImage: String
    let productTitle: String
    let productPrice: String
    var productRating: Double?
    var productReview: String?
    var productDiscount: Int
    var userSaved: String?
    var OriginalPrice: String?
    
    init(model: RecommendedModel) {
        self.productImage = model.productImage
        self.productTitle = model.productTitle
        self.productRating = model.productRating
        self.productReview = "(\(model.productReview ?? 0) Reviews)"
        self.productDiscount = model.productDiscount ?? 0
        if self.productDiscount != 0 {
            self.productPrice = "\(Double(model.productPrice)! - (Double(model.productPrice)! / 100 * Double(model.productDiscount!)))"
            self.userSaved = "\(Double(model.productPrice)! / 100 * Double(model.productDiscount!))"
            self.OriginalPrice = model.productPrice + " EGP"
        }else {
            self.productPrice = model.productPrice + " EGP"
        }
    }
    
    static let recommendedProducts = [
        RecommendedViewModel(model: RecommendedModel(productImage: "shampo", productTitle: "Organic shampoo", productPrice: "120.00", productRating: 2, productReview: 5, productDiscount: 0)),
        RecommendedViewModel(model: RecommendedModel(productImage: "lotion", productTitle: "Organic body lotion KORA", productPrice: "160.00", productRating: 4, productReview: 7, productDiscount: 40)),
        RecommendedViewModel(model: RecommendedModel(productImage: "multi", productTitle: "Multi Vitamin $ extra Zinc", productPrice: "550.00", productRating: 4.5, productReview: 12, productDiscount: 0))
    ]
    
}
