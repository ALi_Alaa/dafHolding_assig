//
//  ReminderViewModel.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 19/05/2021.
//

import Foundation

struct ReminderViewModel {
    
    let reminderImage: String
    let reminderTitle: String
    let reminderSubTitle: String
    
    init(reminderModel: ReminderModel) {
        self.reminderImage = reminderModel.reminderImage
        self.reminderTitle = reminderModel.reminderTitle
        self.reminderSubTitle = reminderModel.reminderSubTitle
    }
    
    static let reminders = [
        ReminderViewModel(reminderModel: ReminderModel(reminderImage: "reminder_bills", reminderTitle: "Doose Reminder", reminderSubTitle: "Next Dosage")),
        ReminderViewModel(reminderModel: ReminderModel(reminderImage: "reminder_appo", reminderTitle: "Appointments", reminderSubTitle: "See Your Next Appointment")),
        
    ]
    
}
