//
//  ReminderModel.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 19/05/2021.
//

import Foundation

struct ReminderModel {
    let reminderImage: String
    let reminderTitle: String
    let reminderSubTitle: String
}
