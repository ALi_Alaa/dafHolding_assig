//
//  Helpers.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 17/05/2021.
//

import UIKit

let SCREEN_WIDTH = UIScreen.main.bounds.size.width

enum CurrentMessageFromHours: String {
    case morning = "Good Morning"
    case noon = "Noon"
    case Afternoon = "Afternoon"
    case evening = "Evening"
}

/// creating programmatic UILabel
/// with writing title , font size, font wight
func createLabel(labelTitle: String, fontSize: CGFloat, fontWeight: UIFont.Weight) -> UILabel {
        let text = UILabel()
        text.text = labelTitle
        text.textColor = .lightGray
        text.font = UIFont.systemFont(ofSize: fontSize, weight: fontWeight)
        return text
}

func createTextField(placeHolderText: String) -> UITextField {
    let txtField = UITextField()
    txtField.textColor = .black
    txtField.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    txtField.attributedPlaceholder =
        NSAttributedString(string: placeHolderText,
                           attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12, weight: .bold)])
    return txtField
}

//MARK:- under line added to textfield
extension UITextField {
    func addUnderLine(width: CGFloat) {
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: 40, width: width, height: 1)
        bottomLine.backgroundColor = UIColor.lightGray.cgColor
        self.borderStyle = UITextField.BorderStyle.none
        self.layer.addSublayer(bottomLine)
    }
}

//MARK:- KeyboardHideFunction
extension UIViewController {
    func hideKeyboardWhenTappedAround(with viewControl: UIView) {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        viewControl.addGestureRecognizer(tapGesture)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
}
