//
//  ServicesCollectionViewCell.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 18/05/2021.
//

import UIKit
import SnapKit

class ServicesCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Identifier
    static let identifier = "Services"
    
    //MARK:- ViewModel
    var serviceViewModel: ServiceViewModel! {
        didSet {
            self.serviceImage.image = UIImage(named: serviceViewModel.serviceImage)
            self.serviceTitle.text = serviceViewModel.serviceTitle.capitalized
        }
    }
    
    //MARK:- Properties
    let background = UIView()
    
    let serviceImage: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = #imageLiteral(resourceName: "trending")
        img.clipsToBounds = true
        return img
    }()
    
    let serviceTitle = createLabel(labelTitle: "", fontSize: 12, fontWeight: .medium)
    
    //MARK:- Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK:- Helpers
    private func configureUI() {
        addSubview(background)
        background.backgroundColor = .white
        background.layer.cornerRadius = 8
        background.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.width.equalToSuperview()
            maker.bottom.equalToSuperview().offset(-30)
        }
        
        background.addSubview(serviceImage)
        serviceImage.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
            maker.width.height.equalTo(40)
        }
        
        addSubview(serviceTitle)
        serviceTitle.text = "hello"
        serviceTitle.textAlignment = .center
        serviceTitle.textColor = .black
        serviceTitle.snp.makeConstraints { (maker) in
            maker.width.equalToSuperview()
            maker.bottom.equalToSuperview().offset(-5)
            maker.centerX.equalToSuperview()
        }
        
    }
    
}
