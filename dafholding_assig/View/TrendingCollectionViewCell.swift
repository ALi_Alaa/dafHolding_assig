//
//  TrendingCollectionViewCell.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 18/05/2021.
//

import UIKit

class TrendingCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Identifier
    static let identifier = "Trending"
    
    //MARK:- Properties
    let trendImage: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFill
        img.image = #imageLiteral(resourceName: "trending")
        img.layer.cornerRadius = 12
        img.clipsToBounds = true
        return img
    }()
    
    //MARK:- Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK:- Helpers
    private func configureUI() {
        addSubview(trendImage)
        trendImage.frame = self.bounds
    }
}
