//
//  LoginView.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 17/05/2021.
//

import UIKit
import SnapKit
import FirebaseAuth

protocol LoginDelegate {
    func signupTappedDelegate()
    func logInApiCallErrorDelegate(error: AuthErrorCode?)
    func logInSuccessDelegate()
}

class LoginView: UIView {

    //MARK:- Delegate
    var delegate: LoginDelegate?
    
    //MARK:- Properties
    let scrollView: UIScrollView = {
      let scroll = UIScrollView()
        return scroll
    }()
    
    let spinner = UIActivityIndicatorView(style: .gray)
    
    let addressEmailLabel = createLabel(labelTitle: "EMAIL ADDRESS", fontSize: 12, fontWeight: .medium)
    
    let emailTextField = createTextField(placeHolderText: "Write your email...")
    
    let passwordLabel = createLabel(labelTitle: "PASSWORD", fontSize: 12, fontWeight: .medium)
    
    let passwordTextField = createTextField(placeHolderText: "your password...")
    
    let signUpLabel = createLabel(labelTitle: "Don't have an account ?", fontSize: 14, fontWeight: .regular)
    
    let signUPButton: UIButton = {
      let btn = UIButton()
        btn.setTitle("Sign up", for: .normal)
        btn.setTitleColor(#colorLiteral(red: 0.1137254902, green: 0.6941176471, blue: 0.9529411765, alpha: 1), for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        btn.addTarget(self, action: #selector(signupTapped), for: .touchUpInside)
        return btn
    }()
    
    let facebookButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "facebook_daf"), for: .normal)
        btn.tintColor = .none
        btn.imageView?.contentMode = .scaleAspectFit
        btn.imageView?.clipsToBounds = true
        return btn
    }()
    
    let gmailButton: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "gmail_daf"), for: .normal)
        btn.tintColor = .none
        btn.imageView?.contentMode = .scaleAspectFit
        btn.imageView?.clipsToBounds = true
        return btn
    }()
    
    let signINButton: UIButton = {
      let btn = UIButton()
        btn.setTitle("Sign In", for: .normal)
        btn.titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .bold)
        btn.backgroundColor = #colorLiteral(red: 0.05490196078, green: 0.4117647059, blue: 0.6549019608, alpha: 1)
        btn.layer.cornerRadius = 11
        btn.isUserInteractionEnabled = true
        btn.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)
        return btn
    }()

    
    //MARK:- Lifecycle
    override func layoutSubviews() {
        super.layoutSubviews()
        configureUI()
    }
    
    //MARK:- Selectors
    @objc
    func signupTapped() {
        delegate?.signupTappedDelegate()
    }
    
    @objc
    func loginTapped() {
        signINButton.setTitle("", for: .normal)
        spinner.startAnimating()
        
        guard !emailTextField.text!.isEmpty, !passwordTextField.text!.isEmpty,
              let email = emailTextField.text, let password = passwordTextField.text else
        {
            ErrorHandler.shared.showError(textLabel: "Please, provide your credintails", alertColor: .systemRed, image: "exclamationmark.triangle.fill", on: self, top: false)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.spinner.stopAnimating()
                self.signINButton.setTitle("Sign In", for: .normal)
            }
        return
        }
        AuthCaller.shared.loginUser(email: email, password: password) { [weak self] result in
            switch result {
            case .failure(let error):
                let err = AuthErrorCode(rawValue: error._code)
                self?.delegate?.logInApiCallErrorDelegate(error: err)
            case .success(let apiResult):
                print("DEBUG: Log in success with user  info: \(apiResult)")
                guard let window = UIApplication.shared.windows.first(where: {$0.isKeyWindow}),
                      let rootController = window.rootViewController as? MainTabController else {return}
                rootController.userCurrentState()
                self?.delegate?.logInSuccessDelegate()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self?.spinner.stopAnimating()
                self?.signINButton.setTitle("Sign In", for: .normal)
            }
        }
    }
    
    //MARK:- Helpers
    private func configureUI() {
        self.addSubview(scrollView)
        scrollView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.left.right.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
        
        scrollView.addSubview(addressEmailLabel)
        addressEmailLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(20)
            maker.width.equalToSuperview()
            maker.left.equalTo(21)
        }
        
        scrollView.addSubview(emailTextField)
        emailTextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(addressEmailLabel.snp.bottom)
            maker.width.equalToSuperview()
            maker.height.equalTo(40)
            maker.left.equalTo(21)
        }
        emailTextField.addUnderLine(width: self.frame.width - 40)
        
        scrollView.addSubview(passwordLabel)
        passwordLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(emailTextField.snp.bottom).offset(33)
            maker.width.equalToSuperview()
            maker.left.equalTo(21)
        }
        
        scrollView.addSubview(passwordTextField)
        passwordTextField.keyboardType = .asciiCapable
        passwordTextField.isSecureTextEntry = true
        passwordTextField.snp.makeConstraints { (maker) in
            maker.top.equalTo(passwordLabel.snp.bottom)
            maker.width.equalToSuperview()
            maker.height.equalTo(40)
            maker.left.equalTo(21)
        }
        passwordTextField.addUnderLine(width: self.frame.width - 40)
        
        let signupStack = UIStackView(arrangedSubviews: [signUpLabel, signUPButton])
        signupStack.axis = .horizontal
        signupStack.alignment = .leading
        signupStack.distribution = .fill
        signupStack.spacing = 6

        signUpLabel.snp.makeConstraints { (maker) in
            maker.height.equalTo(30)
        }
        
        scrollView.addSubview(signupStack)
        signupStack.snp.makeConstraints { (maker) in
            maker.top.equalTo(passwordTextField.snp.bottom).offset(65)
            maker.centerX.equalToSuperview()
        }
        
        let socialMediaStack = UIStackView(arrangedSubviews: [facebookButton, gmailButton])
        socialMediaStack.axis = .horizontal
        socialMediaStack.alignment = .leading
        socialMediaStack.distribution = .fillProportionally
        socialMediaStack.spacing = 12
        
        facebookButton.snp.makeConstraints { maker in
            maker.width.height.equalTo(48)
        }
        
        gmailButton.snp.makeConstraints { maker in
            maker.height.equalTo(48)
            maker.width.equalTo(64)
        }
        
        scrollView.addSubview(socialMediaStack)
        socialMediaStack.snp.makeConstraints { (maker) in
            maker.top.equalTo(signupStack.snp.bottom).offset(33)
            maker.centerX.equalToSuperview()
            maker.height.equalTo(48)
        }
        
        scrollView.addSubview(signINButton)
        signINButton.snp.makeConstraints { (maker) in
            maker.top.equalTo(socialMediaStack.snp.bottom).offset(33)
            maker.centerX.equalToSuperview()
            maker.width.equalTo(340)
            maker.height.equalTo(57)
        }
        
        signINButton.addSubview(spinner)
        spinner.snp.makeConstraints { (maker) in
            maker.center.equalToSuperview()
        }
        
    }
    
}
