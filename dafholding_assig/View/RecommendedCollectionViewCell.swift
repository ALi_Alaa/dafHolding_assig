//
//  RecommendedCollectionViewCell.swift
//  dafholding_assig
//
//  Created by MacBook Pro on 18/05/2021.
//

import UIKit
import Cosmos

class RecommendedCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Identifier
    static let identifier = "Recommended"
    
    var recommendedViewModel: RecommendedViewModel! {
        didSet {
            self.productImage.image = UIImage(named: recommendedViewModel.productImage)
            self.productTitle.text = recommendedViewModel.productTitle
            self.productRating.rating = recommendedViewModel.productRating ?? 0.0
            self.productReview.text = recommendedViewModel.productReview ?? "(0 Review)"
            self.productPrice.text = recommendedViewModel.productPrice
            
            if recommendedViewModel.productDiscount == 0 {
                onDiscount = false
            } else {
             onDiscount = true
                self.discountLabel.text = "-\(recommendedViewModel.productDiscount)%"
                self.productOriginalPrice.text = "\(recommendedViewModel.OriginalPrice!)"
                self.productDiscountLabel.text = "You Saved \(recommendedViewModel.userSaved!) EGP"
            }
        }
    }
    
    //MARK:- Properties
    let background = UIView()
    
    let productImage: UIImageView = {
        let img = UIImageView()
        img.contentMode = .scaleAspectFit
        img.image = #imageLiteral(resourceName: "trending")
        img.clipsToBounds = true
        return img
    }()
    
    let discountLabel: UILabel = {
      let lbl = UILabel()
        lbl.text = "-52%"
        lbl.textAlignment = .center
        lbl.textColor = #colorLiteral(red: 0.1764705926, green: 0.4980392158, blue: 0.7568627596, alpha: 1)
        lbl.font = UIFont.monospacedSystemFont(ofSize: 8, weight: .regular)
        lbl.layer.cornerRadius = 4
        lbl.clipsToBounds = true
        lbl.backgroundColor = #colorLiteral(red: 0.8055006862, green: 0.880856216, blue: 1, alpha: 1)
        return lbl
    }()
    
    let productTitle = createLabel(labelTitle: "", fontSize: 11, fontWeight: .light)
    
    let productRating: CosmosView = {
      let rate = CosmosView()
        rate.settings.updateOnTouch = false
        rate.settings.starSize = 12
        rate.settings.starMargin = 2.5
        return rate
    }()
    
    let productReview = createLabel(labelTitle: "(7 Reviews)", fontSize: 9, fontWeight: .regular)
    
    let productPrice = createLabel(labelTitle: "", fontSize: 12, fontWeight: .bold)
    
    let discountDashLine = UIView()
    
    let productOriginalPrice = createLabel(labelTitle: "10,010.00 EGP", fontSize: 8, fontWeight: .light)
    
    let productDiscountLabel = createLabel(labelTitle: "You Saved 3,160.00 EGP", fontSize: 8, fontWeight: .light)
    
    var onDiscount: Bool = false
    
    //MARK:- Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    //MARK:- Helpers
    private func configureUI() {
        addSubview(background)
        background.backgroundColor = .white
        background.layer.cornerRadius = 8
        background.snp.makeConstraints { (maker) in
            maker.top.bottom.leading.trailing.equalToSuperview()
        }
        
        background.addSubview(productImage)
        productImage.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().offset(6)
            maker.centerX.equalToSuperview()
            maker.width.equalTo(101)
            maker.height.equalTo(65)
        }

        let ratingStack = UIStackView(arrangedSubviews: [productRating, productReview])
        ratingStack.axis = .horizontal
        ratingStack.spacing = 10
        
        let savedStack = UIStackView(arrangedSubviews: [productOriginalPrice, productDiscountLabel])
        savedStack.axis = .vertical
        savedStack.spacing = 4
        
        let recommendedTextStack = UIStackView(arrangedSubviews: [productTitle, ratingStack, productPrice])
        recommendedTextStack.alignment = .leading
        recommendedTextStack.axis = .vertical
        recommendedTextStack.spacing = 8
        
        DispatchQueue.main.async {
            if !self.onDiscount {
                recommendedTextStack.removeArrangedSubview(savedStack)
                self.discountLabel.removeFromSuperview()
            } else {
                recommendedTextStack.addArrangedSubview(savedStack)
                self.background.addSubview(self.discountLabel)
                self.discountLabel.snp.makeConstraints { (maker) in
                    maker.top.equalTo(10)
                    maker.right.equalTo(-16)
                    maker.width.equalTo(27)
                    maker.height.equalTo(15)
                }
                
                self.background.addSubview(self.discountDashLine)
                self.discountDashLine.backgroundColor = .lightGray
                self.discountDashLine.snp.makeConstraints { (maker) in
                    maker.centerY.equalTo(self.productOriginalPrice.snp.centerY)
                    maker.left.equalTo(6)
                    maker.width.equalTo(60)
                    maker.height.equalTo(0.5)
                }
                self.discountDashLine.transform = CGAffineTransform(rotationAngle: 3)
            }
        }
        
        background.addSubview(recommendedTextStack)
        productTitle.text = "Organic Argan Oil For Hair&Shampoo"
        productTitle.textAlignment = .left
        productTitle.textColor = .black
        
        productPrice.text = "650.00 EGP"
        productPrice.textColor = .systemBlue
        
        recommendedTextStack.snp.makeConstraints { (maker) in
            maker.left.equalTo(6)
            maker.right.equalTo(-6)
            maker.top.equalTo(productImage.snp.bottom).offset(8)
        }
        
    }
    
}
